package com.smbcnikkodisco.yell.android.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import android.content.Context;

import com.smbcnikkodisco.yell.android.common.Const;
import com.smbcnikkodisco.yell.android.entity.CompanyMessageLog;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

/**
 * CompanyMessageLogDao
 * 
 * @author cayto inc.
 */
public final class CompanyMessageLogDao extends BaseDao<CompanyMessageLog, String> {

	public CompanyMessageLogDao(Context context) {
		super(context);
		_TAG = Const.TAG + CompanyMessageLogDao.class.getSimpleName();
	}

	@Override
	public CompanyMessageLog create(CompanyMessageLog item) {
		try {
			if (findSizeByMessageId(item.message_id) > 0) {
				return null;
			}
			return super.create(item);
		} catch (Exception e) {
//			Log.w(_TAG, "create item failure");
			return null;
		}
	}

	@Override
	public CompanyMessageLog read(String item_code) {
		CompanyMessageLog item = super.read(item_code);
		return item;
	}

	@Override
	public void delete(CompanyMessageLog item) {
		super.delete(item);
	}

	/**
	 * @param message_id
	 * @return
	 */
	public int findSizeByMessageId(String message_id) {
		try {
			QueryBuilder<CompanyMessageLog, String> qb = getDao().queryBuilder();
			Where<CompanyMessageLog, String> where = qb.where();
			where.eq("message_id", message_id);
			return qb.query().size();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// finish();
		}
	}
	
	/**
	 * @param message_id
	 * @return
	 */
	public int findSizeByMessageIds(ArrayList<String> message_ids) {
		try {
			QueryBuilder<CompanyMessageLog, String> qb = getDao().queryBuilder();
			Where<CompanyMessageLog, String> where = qb.where();
			where.in("message_id", message_ids);
			return qb.query().size();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// finish();
		}
	}
	
	/**
	 * @return
	 */
	public int findSizeByAll() {
		try {
			QueryBuilder<CompanyMessageLog, String> qb = getDao().queryBuilder();
			return qb.query().size();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// finish();
		}
	}

	/**
	 * @return
	 */
	private ArrayList<CompanyMessageLog> findByAll() {
		try {
			QueryBuilder<CompanyMessageLog, String> qb = getDao().queryBuilder();
			qb.orderBy("seq", true); // 昇順
			return new ArrayList<CompanyMessageLog>(qb.query());
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<CompanyMessageLog>();
		}
	}

}