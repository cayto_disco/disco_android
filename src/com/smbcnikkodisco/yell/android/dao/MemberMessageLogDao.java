package com.smbcnikkodisco.yell.android.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.smbcnikkodisco.yell.android.common.Const;
import com.smbcnikkodisco.yell.android.entity.MemberMessageLog;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

/**
 * CompanyMessageLogDao
 * 
 * @author cayto inc.
 */
public final class MemberMessageLogDao extends BaseDao<MemberMessageLog, String> {

	public MemberMessageLogDao(Context context) {
		super(context);
		_TAG = Const.TAG + MemberMessageLogDao.class.getSimpleName();
	}

	@Override
	public MemberMessageLog create(MemberMessageLog item) {
		try {
			if (findSizeByMessageId(item.message_id) > 0) {
				return null;
			}
			return super.create(item);
		} catch (Exception e) {
			return null;
		}
	}
	
	public void create(ArrayList<MemberMessageLog> items) {
		try {
			
			for(MemberMessageLog item : items) {
				super.create(item);
			}
		} catch (Exception e) {
			Log.e("disco", "create failure", e);
		}
	}

	@Override
	public MemberMessageLog read(String item_code) {
		MemberMessageLog item = super.read(item_code);
		return item;
	}

	@Override
	public void delete(MemberMessageLog item) {
		super.delete(item);
	}

	/**
	 * @param message_id
	 * @return
	 */
	public int findSizeByMessageId(String message_id) {
		try {
			QueryBuilder<MemberMessageLog, String> qb = getDao().queryBuilder();
			Where<MemberMessageLog, String> where = qb.where();
			where.eq("message_id", message_id);
			return qb.query().size();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// finish();
		}
	}
	
	/**
	 * @param message_id
	 * @return
	 */
	public int findSizeByMessageIds(ArrayList<String> message_ids) {
		try {
			QueryBuilder<MemberMessageLog, String> qb = getDao().queryBuilder();
			Where<MemberMessageLog, String> where = qb.where();
			where.in("message_id", message_ids);
			return qb.query().size();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// finish();
		}
	}

	/**
	 * @return
	 */
	public ArrayList<MemberMessageLog> findByAll() {
		try {
			QueryBuilder<MemberMessageLog, String> qb = getDao().queryBuilder();
			qb.orderBy("seq", true); // 昇順
			return new ArrayList<MemberMessageLog>(qb.query());
		} catch (SQLException e) {
			Log.d(_TAG,"SQL ERROR ! MemberMessageLog!");
			e.printStackTrace();
			return new ArrayList<MemberMessageLog>();
		}
	}
	
	/**
	 * @param message_id
	 * @return
	 */
	public ArrayList<MemberMessageLog> findAllBySeq() {
		try {
			QueryBuilder<MemberMessageLog, String> qb = getDao().queryBuilder();
			return new ArrayList<MemberMessageLog>(qb.query());
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<MemberMessageLog>();
		} finally {
			// finish();
		}
	}

}