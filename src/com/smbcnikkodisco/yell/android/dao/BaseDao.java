package com.smbcnikkodisco.yell.android.dao;

import static com.smbcnikkodisco.yell.android.common.Const.*;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.ArrayList;

import com.smbcnikkodisco.yell.android.common.DBHelper;
import com.smbcnikkodisco.yell.android.entity.MemberMessageLog;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

/**
 * 
 * @author cayto inc.
 *
 * @param <E>
 * @param <PK>
 */
public abstract class BaseDao<E,PK> {

	protected String _TAG;
	
	protected Context mContext;

	private Class<E> entityClass;
	private DBHelper dbHelper;
	private Dao<E, PK> dao;
	
	@SuppressWarnings("unchecked")
	public BaseDao(Context context) {
		mContext = context;
		ParameterizedType genericSuperclass = (ParameterizedType)getClass().getGenericSuperclass();
		this.entityClass = (Class<E>)genericSuperclass.getActualTypeArguments()[0];
	}

	public E create(E entity) {
		try {
			Dao<E, PK> dao = getDao();
			dao.create(entity);
			return entity;
		} catch (Exception e) {
			return null;
		} finally {
			finish();
		}
	}

	public E read(PK pk) {
		try {
			Dao<E, PK> dao = getDao();
			return dao.queryForId(pk);
		} catch (Exception e) {
			return null;
		} finally {
			finish();
		}
	}

	public void update(E entity) {
		try {
			Dao<E, PK> dao = getDao();
			dao.update(entity);
		} catch (Exception e) {
		} finally {
			finish();
		}
	}

	public void delete(E entity) {
		try {
			Dao<E, PK> dao = getDao();
			dao.delete(entity);
		} catch (Exception e) {
		} finally {
			finish();
		}
	}

	public void store(E entity) {
		try {
			Dao<E, PK> dao = getDao();
			dao.createOrUpdate(entity);
		} catch (Exception e) {
		} finally {
			finish();
		}
	}

	public ArrayList<E> findAll() {
		try {
			Dao<E, PK> dao = getDao();
			return new ArrayList<E>(dao.queryForAll());
		} catch (Exception e) {
			return null;
		} finally {
			finish();
		}
	}

	protected Dao<E, PK> getDao() throws SQLException {
		if (dao != null) {
			return dao;
		}
		if (dbHelper == null) {
			dbHelper = new DBHelper(mContext);
		} else if (!dbHelper.isOpen()) {
			dbHelper = null;
			dbHelper = new DBHelper(mContext);
		}
		dao = dbHelper.getDao(entityClass);
		return dao;
	}

	protected void finish() {

	}

}