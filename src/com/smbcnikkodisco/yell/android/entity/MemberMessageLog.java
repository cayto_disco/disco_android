package com.smbcnikkodisco.yell.android.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "member_message_log")
public final class MemberMessageLog {

	@DatabaseField(id = true)
	public String message_id;			// "12",
//	@DatabaseField(canBeNull = true)
//	public String page;					// null,

    public MemberMessageLog() {}

//	@Override
//	public String toString() {
//		return "CompanyMessageLog [message_id=" + message_id + "]";
//	}

}