package com.smbcnikkodisco.yell.android.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "company_message_log")
public final class CompanyMessageLog {

	@DatabaseField(id = true)
	public String message_id;			// "12",
//	@DatabaseField(canBeNull = true)
//	public String page;					// null,

    public CompanyMessageLog() {}

//	@Override
//	public String toString() {
//		return "CompanyMessageLog [message_id=" + message_id + "]";
//	}

}