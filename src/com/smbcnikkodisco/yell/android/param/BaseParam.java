package com.smbcnikkodisco.yell.android.param;

import java.lang.reflect.ParameterizedType;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.smbcnikkodisco.yell.android.common.Const;

/**
 * BaseParam
 * @author cayto inc.
 */
public abstract class BaseParam {

	public static final String STATUS_SUCCESS = "OK";
	public static final String STATUS_ERROR = "NG";
	
	public BaseParam() {
	}

	public static class Req<REQ> {
		public String show_status = Const._DEV; // 開発環境ステータス
		public String toJson() {
			return new Gson().toJson(this);
		}
	}

	public static class Res<RES> {
		public String status;

		@SuppressWarnings("unchecked")
		public RES toEntity(JSONObject json) {
			ParameterizedType genericSuperclass = (ParameterizedType)getClass().getGenericSuperclass();
			Class<RES> clazz = (Class<RES>)genericSuperclass.getActualTypeArguments()[0];
			return new Gson().fromJson(json.toString(), clazz);
		}

		@Override
		public String toString() {
			return "Res [status=" + status + "]";
		}
		
	}

}