package com.smbcnikkodisco.yell.android.param;

import java.util.ArrayList;

/**
 * ParamMember
 * @author cayto inc.
 */
public class ParamSeniorMessageList extends BaseParam {

	public Req req;
	public Res res;

	public ParamSeniorMessageList() {
		req = new Req();
		res = new Res();
	}

	public static class Req extends BaseParam.Req<Req> {
		public String current; // Request page数
	}

	public static class Res extends BaseParam.Res<Res> {
		public String current;	// Respons page数
		public int total_page; // Max page数
		public ArrayList<ResContent> contents = new ArrayList<ResContent>();

//		@Override
//		public String toString() {
//			return "Res [contents=" + contents + "]";
//		}
		
		public static class ResContent {
			public String id;
			public String title;
			public String image;
			public String thumbnail;
			public String description;
			
			@Override
			public String toString() {
				return "ResRecords [id=" + id + ", title=" + title + ", image=" + image + ",description=" + description + "]";
			}
		}
	}

}