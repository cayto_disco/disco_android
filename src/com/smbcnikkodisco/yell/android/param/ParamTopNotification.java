package com.smbcnikkodisco.yell.android.param;

import java.util.ArrayList;

/**
 * ParamTopNotification
 * @author cayto inc.
 */
public class ParamTopNotification extends BaseParam {

	public Req req;
	public Res res;

	public ParamTopNotification() {
		req = new Req();
		res = new Res();
	}

	public static class Req extends BaseParam.Req<Req> {
		// request param
	}

	public static class Res extends BaseParam.Res<Res> {
		public ResPostCount postCount = new ResPostCount();
		public ResPostId postId = new ResPostId();
		public ResContentsData contentsData = new ResContentsData();

		@Override
		public String toString() {
			return "Res [seminerPostCount=" + postCount.seminerPostCount + "companyPostCount=" + postCount.companyPostCount + "employeePostCount=" + postCount.employeePostCount + "]";
		}
		
		public static class ResPostCount {
			public int seminerPostCount;
			public int companyPostCount;
			public int employeePostCount;
		}
		
		public static class ResPostId {
			public String employeePostId;
			public String companyPostId;
			public String seminerPostId;
		}
		
		public static class ResContentsData {
			public String career;
			public String question;
			public String mypage;
			public String finance;
			public String homepage;
		}
	}

}