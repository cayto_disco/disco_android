/**
 * Android2.2~ なのでDefaultHttpClientを使用
 * HttpURLConnectionは2.2以下ではバグが含まれている可能性がある
 */
package com.smbcnikkodisco.yell.android.common;

import static com.smbcnikkodisco.yell.android.common.Const.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 
 * @author cayto inc.
 */
public final class ComHttp {

	private static final String _TAG = Const.TAG + ComHttp.class.getSimpleName();
	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isConnected(Context context) {
		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = manager.getActiveNetworkInfo();

		return (networkInfo != null && networkInfo.isConnected());
	}

	public static String get(String url) {
		 
		HttpURLConnection conn = null;
		Scanner scanner = null;
		try {
			// create connection
			conn = (HttpURLConnection)new URL(url).openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(Const.HTTP_CONN_TIMEOUT);
			conn.setReadTimeout(Const.HTTP_SOCKET_TIMEOUT);

			conn.connect();

			int statusCode = conn.getResponseCode();
			if (statusCode != HttpURLConnection.HTTP_OK) {
				return null;
			}

			InputStream in = new BufferedInputStream(conn.getInputStream());
			scanner = new Scanner(in);
			return scanner.useDelimiter("\\A").next();
		    } catch (MalformedURLException e) {
		        // URL is invalid
		    } catch (SocketTimeoutException e) {
		    	// data retrieval or connection timed out
		    } catch (IOException e) {
		        // could not read response body 
		        // (could not create input stream)
		    } finally {
		    	if (scanner != null) {
		    		scanner.close();
		    	}
		        if (conn != null) {
		            conn.disconnect();
		        }
		    }       
		    return null;
	}

	public static String getZip(String url) {
		HttpURLConnection conn = null;
		try {
			// HTTP接続開始
			conn = (HttpURLConnection) new URL(url).openConnection();
			conn.setRequestMethod("GET");
			conn.connect();

			// テンポラリファイルの設定
			File outputFile = File.createTempFile("temp", ".zip");
			FileOutputStream fos = new FileOutputStream(outputFile);

			// ダウンロード開始
			InputStream is = conn.getInputStream();
			byte[] buffer = new byte[1024];
			int len = 0;

			while ((len = is.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}

			fos.close();
			is.close();
			// ダウンロードした画像ファイルのパスを返す
			return outputFile.getPath();
		} catch (IOException e) {
			e.printStackTrace();
			// エラーの場合nullを返す
			return null;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}


	public static String post(String url, String params) {
		 
		HttpURLConnection conn = null;
		Scanner scanner = null;
		try {
			// create connection
			conn = (HttpURLConnection)new URL(url).openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(Const.HTTP_CONN_TIMEOUT);
			conn.setReadTimeout(Const.HTTP_SOCKET_TIMEOUT);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setFixedLengthStreamingMode(params.getBytes().length);
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");

			OutputStream os = conn.getOutputStream();
			os.write(params.getBytes("UTF-8"));
			os.close();
			os.flush();

			conn.connect();

			int statusCode = conn.getResponseCode();
			if (statusCode != HttpURLConnection.HTTP_OK) {
				return null;
			}

			InputStream in = new BufferedInputStream(conn.getInputStream());
			scanner = new Scanner(in);
			return scanner.useDelimiter("\\A").next();
		    } catch (MalformedURLException e) {
		        // URL is invalid
		    } catch (SocketTimeoutException e) {
		    	// data retrieval or connection timed out
		    } catch (IOException e) {
		        // could not read response body 
		        // (could not create input stream)
		    } finally {
		    	if (scanner != null) {
		    		scanner.close();
		    	}
		        if (conn != null) {
		            conn.disconnect();
		        }
		    }       
		    return null;
	}

	public static String _post(final String uri, final String params, final CountDownLatch latch) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost request = new HttpPost(uri);
		request.setHeader("Content-type", "application/json");

		try {
			request.setEntity(new StringEntity(params, CHAR_SET));

			String result = httpClient.execute(request, new ResponseHandler<String>() {
				@Override
				public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {

//					latch.countDown();

					switch (response.getStatusLine().getStatusCode()) {
					case HttpStatus.SC_OK:
						// HttpStatus 200
						return EntityUtils.toString(response.getEntity(), CHAR_SET);
					case HttpStatus.SC_NOT_FOUND:
						throw new RuntimeException("404!"); // FIXME
					default:
						throw new RuntimeException("ERROR!"); // FIXME
					}
				}
			});
			return result;
		} catch (ClientProtocolException e) {
			throw new RuntimeException(e); // FIXME
		} catch (IOException e) {
			throw new RuntimeException(e); // FIXME
		} catch (Exception e) {
			throw new RuntimeException(e); // FIXME
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
	}

	// 後で定数化
	final static int COUNT_OF_CON_TOIMEOUT = 15 * 1000;
	final static int COUNT_OF_SO_TIMEOUT = 15 * 1000;

}