package com.smbcnikkodisco.yell.android.common;

import android.net.Uri.Builder;

public final class Const {
	
	// 開発環境設定
//	public static final String _DEV = "production";
	public static final String _DEV = "dev";
	
	private static final String _SCHEME = "http";
	private static final String _DOMAIN = "disco-smbcns.com";
	private static final String _API_DOMAIN = "api.smbcnikko-disco.com";

	public static final Builder _URI_API;

	static {
		_URI_API = new Builder();
		_URI_API.scheme(_SCHEME);
		_URI_API.encodedAuthority(_API_DOMAIN);
	}

	public static final String TAG = "disco_smbcns-";
	public static final String CHAR_SET = "UTF-8";

	public static final String HTTP_KEY_URL = "url";
	public static final String HTTP_KEY_PARAM = "param";

	public static final int HTTP_CONN_TIMEOUT = 15 * 1000;
	public static final int HTTP_SOCKET_TIMEOUT = 15 * 1000;

	public static final String API_URL_ = _URI_API.toString();
	public static final String API_URL_MEMBER_MESSAGE_LIST	= API_URL_ + "/member/";
	public static final String API_URL_COMPANY_MESSAGE_LIST	= API_URL_ + "/company/";
	public static final String API_URL_SEMINAR_LIST 		= API_URL_ + "/seminar/";
	public static final String API_URL_POST_COUNT			= API_URL_ + "/postcount/";

	public static final String LOCAL_URL_ = "file:///android_asset/htmls/";
	public static final String LOCAL_URL_ERROR = LOCAL_URL_ + "error.html";

	public static final String DATABASE_NAME = "db_disco_smbcns";

	public static final String BR = System.getProperty("line.separator");

	public static class Extra {
		public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
	}
}
