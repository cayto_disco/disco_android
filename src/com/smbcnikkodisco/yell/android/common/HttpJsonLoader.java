/**
 * 
 */
package com.smbcnikkodisco.yell.android.common;

import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_PARAM;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_URL;

import java.io.FileDescriptor;
import java.io.PrintWriter;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;

/**
 * 
 * @author cayto inc.
 */
public final class HttpJsonLoader extends AsyncTaskLoader<JSONObject> {

	private static final String _TAG = Const.TAG + HttpJsonLoader.class.getSimpleName();

	private final String url;
	private String params;
	private JSONObject result;

	/**
	 * constractor.
	 * 
	 * @param context
	 * @param url
	 * @param param
	 */
	public HttpJsonLoader(Context context, Bundle args) {
		super(context);
		url = args.getString(HTTP_KEY_URL);
		params = args.getString(HTTP_KEY_PARAM);
	}

	@Override
	public JSONObject loadInBackground() {
		if (TextUtils.isEmpty(url)) {
			return null;
		}
		String result = null;
		if (TextUtils.isEmpty(params)) {
			// get
			result = ComHttp.get(url);
		} else {
			// post
//			result = doPost();
			result = ComHttp.post(url, params);
		}
		if (TextUtils.isEmpty(result)) {
			// not result
//			Log.w(_TAG, "empty json");
			return null;
		}
		try {
			JSONObject json = new JSONObject(result);
			return json;
		} catch (JSONException e) {
//			Log.e(_TAG, "error json", e);
			return null;
		}
	}

	@Override
	public void deliverResult(JSONObject data) {
		result = data;
		// reset
		if (isReset()) {
			result = null;
			return;
		}
		// started
		if (isStarted()) {
			super.deliverResult(data);
		}
	}

	@Override
	protected void onStartLoading() {
		if (result != null) {
			deliverResult(result);
		}
		if (takeContentChanged() || result == null) {
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		super.onStopLoading();
		cancelLoad();
	}

	@Override
	protected void onReset() {
		super.onReset();
		onStopLoading();
	}

	@Override
	public void dump(String prefix, FileDescriptor fd, PrintWriter writer, String[] args) {
		super.dump(prefix, fd, writer, args);
		writer.print(prefix);
		writer.print("url=");
		writer.println(url);
		writer.print(prefix);
		writer.print("result=");
		writer.println(result);
	}
}