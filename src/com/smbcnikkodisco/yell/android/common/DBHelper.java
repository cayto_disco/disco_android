package com.smbcnikkodisco.yell.android.common;

import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.smbcnikkodisco.yell.android.entity.CompanyMessageLog;
import com.smbcnikkodisco.yell.android.entity.MemberMessageLog;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * 
 * @author cayto inc.
 */
public final class DBHelper extends OrmLiteSqliteOpenHelper {

	private static final int DATABASE_VERSION = 1;

	public DBHelper(Context context) {
		super(context, Const.DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
		try {
			// create table
			TableUtils.createTable(connectionSource, CompanyMessageLog.class);
			TableUtils.createTable(connectionSource, MemberMessageLog.class);
		} catch (SQLException e) {
//			Log.e(TAG, "create table failure", e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
		// alter table
	}

}