package com.smbcnikkodisco.yell.android.common;

import java.util.EnumMap;

import com.smbcnikkodisco.yell.android.fragment.*;

/**
 * FragmentId
 * @author cayto inc.
 */
public enum FragmentId {

	INIT,
	WEB_VIEW,
	TOP_PAGE,
	MESSAGE_LIST_FROM_COMPANY,
	MESSAGE_LIST_FROM_MEMBER,
	SEMINAR_LIST,
	MESSAGE_FROM_MEMBER,
	IMAGE_PAGER,
	;

	/**
	 * 
	 * @return
	 */
	public Class<? extends BaseFragment> getFragment() {
		return FRAGMENTS.get(this);
	}

	//
	private static final EnumMap<FragmentId, Class<? extends BaseFragment>> FRAGMENTS;

	static {
		//
		FRAGMENTS = new EnumMap<FragmentId, Class<? extends BaseFragment>>(FragmentId.class);
		FRAGMENTS.put(INIT, null);
		FRAGMENTS.put(WEB_VIEW, WebViewFragment.class);
		FRAGMENTS.put(TOP_PAGE,TopPageFragment.class);
		FRAGMENTS.put(MESSAGE_LIST_FROM_COMPANY, MessageListFromCompanyFragment.class);
		FRAGMENTS.put(MESSAGE_LIST_FROM_MEMBER, MessageListFromMemberFragment.class);
		FRAGMENTS.put(SEMINAR_LIST, SeminarListFragment.class);
		FRAGMENTS.put(MESSAGE_FROM_MEMBER, MessageFromMemberFragment.class);
		FRAGMENTS.put(IMAGE_PAGER, ImagePagerFragment.class);
	}

}