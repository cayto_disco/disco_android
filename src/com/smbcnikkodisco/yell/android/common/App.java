package com.smbcnikkodisco.yell.android.common;

import java.io.File;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.LimitedAgeDiscCache;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.smbcnikkodisco.yell.android.content.NutraBaseImageDecoder;

/**
 * App
 * 
 * @author cayto inc.
 */
public final class App extends Application {

	private static final String _TAG = Const.TAG + App.class.getSimpleName();

	private static final String _KEY_IS_BASE_DATA = "is_base_data";

	private Context mContext;
	private SharedPreferences mPrefs;

	@Override
	public void onCreate() {
		mContext = getApplicationContext();
		mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		// イメージローダー初期化
		initImageLoader(mContext);
	}

	/**
	 * 
	 */
	public void createBaseData() {
		if (isBaseData()) {
			return;
		}
		fixBaseData();
	}

	/**
	 * 
	 */
	public void cleanData() {
		// キャッシュ削除
		clearImageLoaderCache();
	}


	private static void initImageLoader(Context context) {
		File cacheDir = StorageUtils.getCacheDirectory(context);
		ImageLoaderConfiguration config =
				new ImageLoaderConfiguration.Builder(context)
											.threadPoolSize(5)
											.imageDecoder(new NutraBaseImageDecoder(true))
											.diskCache(new LimitedAgeDiscCache(cacheDir, 24 * 60 * 60 * 1000))
											.discCacheSize(10 * 1024 * 1024)
											.denyCacheImageMultipleSizesInMemory()
											.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
											.build();

		ImageLoader.getInstance().init(config);
	}

	private static void clearImageLoaderCache() {
		try {
			ImageLoader imageLoader = ImageLoader.getInstance();
			DiskCache diskCache = imageLoader.getDiskCache();
			diskCache.clear();
		} catch (Exception e) {
		}
	}

	public void fixBaseData() {
		savePrefs(_KEY_IS_BASE_DATA, "ok");
	}

	public boolean isBaseData() {
		String flg = loadPrefs(_KEY_IS_BASE_DATA);
		return !TextUtils.isEmpty(flg);
	}

	/***
	 * 
	 * @return
	 */
	private String loadPrefs(String key) {
		return mPrefs.getString(key, null);
	}

	/**
	 * 
	 * @param key
	 */
	private void savePrefs(String key, String data) {
		Editor editor = mPrefs.edit();
		editor.putString(key, data);
		editor.commit();
	}

}