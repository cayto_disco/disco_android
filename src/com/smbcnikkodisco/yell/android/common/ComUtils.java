package com.smbcnikkodisco.yell.android.common;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class ComUtils {

	private static final String ALGORISM = "hmacSHA256";

	public static String getHash(String str, String salt) {
		SecretKeySpec secretKeySpec = new SecretKeySpec(salt.getBytes(), ALGORISM);
		try {
			Mac mac = Mac.getInstance(ALGORISM);
			mac.init(secretKeySpec);
			byte[] result = mac.doFinal((str + "_disco").getBytes());
			return byteToString(result);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		return "";
	}

	private static String byteToString(byte[] b) {
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < b.length; i++) {
			int d = b[i];
			d += (d < 0) ? 256 : 0;
			if (d < 16) {
				buffer.append("0");
			}
			buffer.append(Integer.toString(d, 16));
		}
		return buffer.toString();

	}
	 
	/**
	 * Byte配列を文字列に変換する. 0x10以下の時は、0をつけて表示する.
	 * @param hash MD5変換後のbyte配列
	 * @return MD5変換後の文字列
	 */
	public static String hashByteToMD5(byte[] hash) {
	    StringBuilder builder = new StringBuilder();
	    for (int idx = 0; idx < hash.length; idx++) {
	        if ((0xff & hash[idx]) < 0x10) {
	            builder.append("0" + Integer.toHexString((0xff & hash[idx])));
	        } else {
	            builder.append(Integer.toHexString((0xff & hash[idx])));
	        }
	    }
	    return builder.toString();
	}
}