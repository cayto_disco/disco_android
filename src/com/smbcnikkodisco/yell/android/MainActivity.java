package com.smbcnikkodisco.yell.android;

import static com.smbcnikkodisco.yell.android.common.Const.TAG;
import static com.smbcnikkodisco.yell.android.common.FragmentId.TOP_PAGE;
import net.app_c.cloud.sdk.AppCCloud;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.Fragment.InstantiationException;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.smbcnikkodisco.yell.android.common.App;
import com.smbcnikkodisco.yell.android.fragment.TopPageFragment;

import com.smbcnikkodisco.yell.android.R;

/**
 * MainActivity
 * 
 * @author cayto inc.
 */
public final class MainActivity extends ActionBarActivity {

	private final static String _TAG = TAG + MainActivity.class.getSimpleName();
	
	// バックキーコールバック
	public interface OnBackPressedListener {
		void onBackPressed();
	}

	// スプラッシュ表示時間
	private static final int _SPLASH_DELAY = 1 * 1000; // 1秒
	
	// progress
	private ProgressBar mProgressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// ここでアクションバーのテーマをセット
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mProgressBar = (ProgressBar)findViewById(R.id.progress_bar);
		mProgressBar.setVisibility(View.GONE);
		
		// スタート時刻保持
		long startTime = System.currentTimeMillis();
		// アクションバー初期化
		initActionBar();
		// appc cloud start on push
		new AppCCloud(this).on(AppCCloud.API.PUSH).start();
		// Push 制御　非表示
		cancelNotification();
		
		if (savedInstanceState == null) {
			// 初期化
			initBaseData();
			doLaunch(getIntent());
		}

		// スプラッシュ制御
		long delay = _SPLASH_DELAY - (System.currentTimeMillis() - startTime);
//		Log.d(_TAG, "delay " + delay);
		delay = delay < 0 ? 0 : delay;
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void finish() {
		try {
			App app = (App)getApplication();
			app.cleanData();
			Thread.sleep(100);
		} catch (InterruptedException e) {}
		super.finish();
	}

	@Override
	public void onBackPressed() {
		// バックキー
		Fragment currentFragment = getCurrentFragment();
		if (currentFragment != null) {
			if (currentFragment instanceof TopPageFragment) {
				// マイテンプレートなら終了
				finish();
				return;
			}
			// カレントフラグメントへバックキー通知
			if (currentFragment instanceof OnBackPressedListener) {
				((OnBackPressedListener) currentFragment).onBackPressed();
				return;
			}
		}
		// カレントフラグメントが存在しなければ終了
		finish();
	}

	@Override
	public void onNewIntent(Intent intent) {
//		Log.d(_TAG, "onNewIntent");
		doLaunch(intent);
	}

	/**
	 * カレントフラグメント取得
	 * @return
	 */
	public Fragment getCurrentFragment() {
		try {
			int count = getSupportFragmentManager().getBackStackEntryCount();
			String tag = getSupportFragmentManager().getBackStackEntryAt(count - 1).getName();
			return getSupportFragmentManager().findFragmentByTag(tag);
		} catch(Exception e) {
			return null;
		}
	}

	/**
	 * トップフラグメント表示
	 * @param fragmentId
	 */
	private void showTopFragment() {
		try {
			TopPageFragment fragment = TopPageFragment.newInstance();
			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			// animation
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(R.id.container, fragment, fragment.getFragmentId().toString());
			transaction.addToBackStack(fragment.getFragmentId().toString());
			transaction.commitAllowingStateLoss();
		} catch (InstantiationException e) {}
	}

	private void initActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setTitle("");
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		actionBar.setCustomView(R.layout.action_bar_title);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setHomeButtonEnabled(true);
	}

	//
	private void initBaseData() {
		App app = (App) getApplication();
		// 基本データ作成
		app.createBaseData();
	}

	//
	private void doLaunch(Intent intent) {
		// 通常起動
		TopPageFragment topPageFragment = (TopPageFragment) getSupportFragmentManager().findFragmentByTag(TOP_PAGE.toString());
		if (topPageFragment == null) {
			showTopFragment();
		}
	}
	
	public void showProgress() {
		if (mProgressBar != null && mProgressBar.getVisibility() == View.GONE) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mProgressBar.setVisibility(View.VISIBLE);
				}
			});
		}
	}

	public void hideProgress() {
		if (mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mProgressBar.setVisibility(View.GONE);
				}
			});
		}
	}
	
	public boolean isProgress() {
		
		return mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE;
	}
	
	// PUSH通知を削除する
	public void cancelNotification() {
	    String ns = getApplicationContext().NOTIFICATION_SERVICE;
	    NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
	    nMgr.cancelAll();
	}
}