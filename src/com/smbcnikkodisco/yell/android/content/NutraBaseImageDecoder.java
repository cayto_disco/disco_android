package com.smbcnikkodisco.yell.android.content;

import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.io.IOException;
import java.io.InputStream;

import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.decode.ImageDecodingInfo;

import java.io.File;
import java.io.IOException;

import magick.ColorspaceType;
import magick.ImageInfo;
import magick.MagickImage;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.nostra13.universalimageloader.cache.disc.DiscCacheAware;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.decode.ImageDecodingInfo;
import com.smbcnikkodisco.yell.android.fragment.MessageListFromMemberFragment;

public class NutraBaseImageDecoder extends BaseImageDecoder {

	private ImageInfo info;
	private MagickImage imageCMYK;
	protected String _TAG;
	
    public NutraBaseImageDecoder(boolean loggingEnabled) {
        super(loggingEnabled);
        _TAG = TAG + NutraBaseImageDecoder.class.getSimpleName();
    }

	@Override
	public Bitmap decode(ImageDecodingInfo decodingInfo)
	{
		Bitmap result = null;
		
		try {
			result = super.decode(decodingInfo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//base decoder may have failed CMYK images - lets try our own and if it fails return null
		if(result == null)
		{
			Log.d(_TAG, "MY DECODER IN USE");
			
        	try
        	{
        		DiscCacheAware discCache = ImageLoader.getInstance().getDiscCache();
        		File imageFile = discCache.get(decodingInfo.getImageUri());
        		
        		File cachedImageFile = ImageLoader.getInstance().getDiscCache().get(decodingInfo.getImageUri());
        		Log.d(_TAG, "ImageUri : " + decodingInfo.getImageUri());
                if (!cachedImageFile.exists()) {
                	Log.d(_TAG, "FILE DOES NOT EXIST");
                    return null;
                }
        		
        		//File test = new File(decodingInfo.getImageUri());
        		
        		info = new ImageInfo(imageFile.getAbsolutePath());
        		imageCMYK = new MagickImage(info);
        		
        		if(imageCMYK.getColorspace() == ColorspaceType.CMYKColorspace)
        		{
        			Log.d(_TAG, "Image is CMYK decode using My decoder");
        			
		        	boolean imgConvertStatus = imageCMYK.transformRgbImage(ColorspaceType.CMYKColorspace);					        		
		        		
	        		if(imgConvertStatus)
	        		{
	        			Log.v("App", "Image successfully decoded using My decoder");
	        			imageCMYK.setFileName(imageFile.getAbsolutePath());
	        			imageCMYK.writeImage(info);
	        			
	        			Log.d("App", "Loading saved image from file system as a Bitmap");
	        			
	        			BitmapFactory.Options options = new BitmapFactory.Options();
	        			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	        			
	        			Bitmap bitmap = BitmapFactory.decodeFile(decodingInfo.getImageUri(), decodingInfo.getDecodingOptions());
	        			
	        			Log.d(_TAG, "Loaded bitmap");
	        			
	        			result = bitmap;
	        		}
	        		else
	        		{
	        			return null;
	        		}		
        		}
        	}
        	catch(Exception e)
        	{
        		return null;
        	}
		}
		
		return result;
	}
	
	

    @Override
    protected InputStream getImageStream(ImageDecodingInfo decodingInfo) throws IOException {
        InputStream stream = decodingInfo.getDownloader()
                .getStream(decodingInfo.getImageUri(), decodingInfo.getExtraForDownloader());
        return stream == null ? null : new JpegClosedInputStream(stream);
    }

    private class JpegClosedInputStream extends InputStream {

        private static final int JPEG_EOI_1 = 0xFF;
        private static final int JPEG_EOI_2 = 0xD9;
        private final InputStream inputStream;
        private int bytesPastEnd;

        private JpegClosedInputStream(final InputStream iInputStream) {
            inputStream = iInputStream;
            bytesPastEnd = 0;
        }

        @Override
        public int read() throws IOException {
            int buffer = inputStream.read();
            if (buffer == -1) {
                if (bytesPastEnd > 0) {
                    buffer = JPEG_EOI_2;
                } else {
                    ++bytesPastEnd;
                    buffer = JPEG_EOI_1;
                }
            }

            return buffer;
        }
    }
}