package com.smbcnikkodisco.yell.android.fragment;

import com.smbcnikkodisco.yell.android.MainActivity;
import com.smbcnikkodisco.yell.android.R;
import com.smbcnikkodisco.yell.android.MainActivity.OnBackPressedListener;
import com.smbcnikkodisco.yell.android.common.App;
import com.smbcnikkodisco.yell.android.common.ComHttp;
import com.smbcnikkodisco.yell.android.common.Const;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.smbcnikkodisco.yell.android.param.BaseParam;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

/**
 * BaseFragment
 * 
 * @author cayto inc.
 */
public abstract class BaseFragment extends Fragment implements OnBackPressedListener {

	protected String _TAG;

	private static final int _REQ_CODE_ERROR_CONN = 2002;

	private BaseFragment mCalledFragment;	// 呼び出し元フラグメント
	private MainActivity mMainActivity;		// MainActivityアクティビティ
	private Boolean mCloseFlg;				// 閉じるフラグ(NULLあり)

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (mMainActivity == null) {
			mMainActivity = (MainActivity) activity;
		}
		// アクションバー編集
		editActionBar();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		// スクリーンショットを無効にする
//		mMainActivity.getWindow().setFlags(LayoutParams.FLAG_SECURE, LayoutParams.FLAG_SECURE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		// 通信エラー
		if (requestCode == _REQ_CODE_ERROR_CONN && resultCode == Activity.RESULT_OK) {
			// 再通信
			getMainActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					reload();
				}
			});
			return;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onStart() {
//		 Log.d(_TAG, "onStart");
		super.onStart();
	}

	@Override
	public void onResume() {
//		 Log.d(_TAG, "onResume");
		super.onResume();
	}

	@Override
	public void onPause() {
//		 Log.d(_TAG, "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
//		 Log.d(_TAG, "onStop");
		super.onStop();
	}

	@Override
	public void onDestroyView() {
//		 Log.d(_TAG, "onDestroyView");
		super.onDestroyView();
		// キーボードを閉じる
		hideKeyboard();
	}

	@Override
	public void onDestroy() {
//		 Log.d(_TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onDetach() {
//		 Log.d(_TAG, "onDetach");
		super.onDetach();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
//		inflater.inflate(R.menu.close, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case R.id.close:
//			closeFragment();
//			return true;
//		case android.R.id.home:
//			getActivity().onBackPressed();
//			return true;
//		}
		return true;
	}

	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		// 開く
		if (transit == FragmentTransaction.TRANSIT_FRAGMENT_OPEN) {
			if (enter) {
				// イン
				return AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_slide_in_right);
			} else {
				// アウト
				return AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_slide_out_left);
			}
		}
		// 閉じる 
		if (transit == FragmentTransaction.TRANSIT_FRAGMENT_CLOSE) {
			if (enter) {
				// イン
				return AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_slide_in_left);
			} else {
				// アウト
				if (mCloseFlg == null) {
					return AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_slide_non);
				} else if (mCloseFlg) {
					return AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_slide_out_bottom);
				} else {
					return AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_slide_out_rignt);
				}
			}
		}
		return super.onCreateAnimation(transit, enter, nextAnim);
	}

	@Override
	public void onBackPressed() {
		// バックキー処理
		mCloseFlg = false;
		FragmentManager fragmentManager = getMainActivity().getSupportFragmentManager();
		// フラグメントバック
		fragmentManager.popBackStack();
		// 呼び出し元フラグメントを再表示
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.show(mCalledFragment); // 再表示
		transaction.commitAllowingStateLoss();

		if (mCalledFragment != null) {
			// 呼び出し元フラグメント更新
			getMainActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// UI更新もあるのでUIスレッドで呼び出し
					mCalledFragment.active();
//					Log.d(_TAG, "call active " + mCalledFragment.getFragmentId());
				}
			});
		} else {
			// カレントフラグメントがなければアプリ終了
			getMainActivity().finish();
		}
		// 自身をスリープ処理
		sleep();
	}

	/**
	 * フラグメント追加
	 * @param fragmentId
	 */
	public void addFragment(final FragmentId fragmentId) {
		addFragment(fragmentId, null);
	}

	/**
	 * フラグメント追加
	 * @param fragmentId
	 */
	public synchronized void addFragment(final FragmentId nextFragmentId, Bundle bundle) {
		if (isLock()) {
			// ロック中なので終了
			return;
		}
		try {
			FragmentManager manager = getMainActivity().getSupportFragmentManager();
			BaseFragment nextFragment = (BaseFragment) nextFragmentId.getFragment().newInstance();
			nextFragment.setArguments(bundle);
			nextFragment.mCalledFragment = (BaseFragment) manager.findFragmentByTag(getFragmentId().toString());
			FragmentTransaction transaction = manager.beginTransaction();
			// animation
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(R.id.container, nextFragment, nextFragmentId.toString());
			// 非表示
			transaction.hide(this);
			// メニューを透過してくるので呼び出し元はメニュー無効
			this.setHasOptionsMenu(false);
			transaction.addToBackStack(nextFragmentId.toString());
			transaction.commitAllowingStateLoss();
		} catch (InstantiationException e) {
//			Log.e(_TAG,	"InstantiationException", e);
		} catch (IllegalAccessException e) {
//			Log.e(_TAG,	"IllegalAccessException", e);
		} catch (java.lang.InstantiationException e) {
//			Log.e(_TAG,	"InstantiationException", e);
		}
	}

	/**
	 * MainActivity取得.
	 * MainActivityがNULLにならないようにガンバルメソッド
	 * @return MainActivity
	 */
	protected MainActivity getMainActivity() {
		if (mMainActivity == null) {
			return (MainActivity) getActivity();
		}
		return mMainActivity;
	}

	/**
	 * リソースIDから文字列取得.
	 * getActivity()が稀にNULLを返すため、getMainActivity()から取得
	 * @param id
	 * @return String
	 */
	protected String getStr(int id) {
		return getMainActivity().getString(id);
	}

	//
	protected String getCurrentFragmentTag() {
		try {
			int count = getMainActivity().getSupportFragmentManager().getBackStackEntryCount();
			return getMainActivity().getSupportFragmentManager().getBackStackEntryAt(count - 1).getName();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * フラグメントを閉じる
	 */
	protected void closeFragment() {
		mCloseFlg = true;
		// 閉じる処理
		FragmentManager manager = getMainActivity().getSupportFragmentManager();
		// スリープ処理
		sleep();
		// フラグメントの最初まで戻る
		manager.popBackStack(1, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		final TopPageFragment myTemplateFragment = (TopPageFragment) getMainActivity().getSupportFragmentManager().findFragmentByTag(FragmentId.TOP_PAGE.toString());
		if (myTemplateFragment != null) {
			getMainActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// マイテンプレートがあれば更新
					myTemplateFragment.active();
				}
			});
		} else {
			// アプリ終了
			getMainActivity().finish();
		}
	}

	/**
	 * アクティブ(更新)処理
	 */
	protected void active() {
		// アクションバー編集
		editActionBar();
		// メニュー有効
		setHasOptionsMenu(true);
	}

	/**
	 * スリープ処理
	 */
	protected void sleep() {
		setHasOptionsMenu(false); // メニュー無効
	}

	/**
	 * リロード処理
	 */
	protected void reload() {}

	/**
	 * 
	 * @return
	 */
	protected boolean isLock() {
		return isHidden() || getMainActivity().isProgress();
	}

	/**
	 * キーボード非表示
	 * @param textView
	 */
	protected void hideKeyboard() {
		Activity activity = getMainActivity();
		try {
			if (activity.getCurrentFocus() != null) {
				InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
			}
		} catch (Exception e) {
			// Log.e(_TAG, "hide keyboard", e);
		}
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	protected boolean checkParam(JSONObject json) {
		if (json == null || TextUtils.isEmpty(json.toString())) {
			// jsonが空
//			showConfirmDialog(getStr(R.string.msg_not_network), getStr(R.string.label_reload), getStr(R.string.label_cancel), null, _REQ_CODE_ERROR_CONN);

			return false;
		}
		try {
			String status = json.getString("status");

			if (BaseParam.STATUS_SUCCESS.equals(status)) {
				// 処理正常
				return true;
			}

			return true;
		} catch (Exception e) {
//			Log.e(_TAG, "json error", e);
			// // showConfirmDialog(getStr(R.string.msg_not_network),
			// getStr(R.string.label_reload), getStr(R.string.label_cancel),
			// null, _REQ_CODE_ERROR_CONN);

			return false;
		}
	}
	
	public static void showConfirmOpenBrowserDialog(final Fragment fragment,final Bundle bundle){
		
        // 確認ダイアログの生成
        AlertDialog.Builder alertDlg = new AlertDialog.Builder(fragment.getActivity());
        alertDlg.setTitle(bundle.getString("title"));
        alertDlg.setMessage("ブラウザを開きます。\nよろしいですか？");
        
        alertDlg.setPositiveButton(
            "ブラウザを開く",
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
    				Uri uri = Uri.parse(bundle.getString("link"));
    				Intent i = new Intent(Intent.ACTION_VIEW,uri);
    				fragment.startActivity(i); 
                }
            });
        alertDlg.setNegativeButton(
            "キャンセル",
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Cancel ボタンクリック処理
                }
            });

        // 表示
        alertDlg.create().show();
	}

	//
	private void editActionBar() {
		ActionBar actionBar = getMainActivity().getSupportActionBar();
		actionBar.setLogo(getIcon());
	}

	/**
	 * フラグメントID
	 * @return
	 */
	public abstract FragmentId getFragmentId();

	/**
	 * タイトル
	 * @return
	 */
	public abstract String getTitle();

	/**
	 * アイコン
	 * @return
	 */
	public abstract int getIcon();

}