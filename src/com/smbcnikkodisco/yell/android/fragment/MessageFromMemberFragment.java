package com.smbcnikkodisco.yell.android.fragment;

import static com.smbcnikkodisco.yell.android.common.Const.TAG;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smbcnikkodisco.yell.android.R;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * MessageFromMemberFragment
 * 
 * @author cayto inc.
 */
public final class MessageFromMemberFragment extends BaseFragment {

	private TextView mTextViewTitle;
	private ImageView mImageView;
//	private TextView mTextViewDescription;
	private ImageLoader mImageLoader;
	private DisplayImageOptions mImageOptions;
	
	public MessageFromMemberFragment() {
		super();
		_TAG = TAG + MessageFromMemberFragment.class.getSimpleName();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_message_from_member, container, false);
		mImageLoader = ImageLoader.getInstance();
		mImageOptions = new DisplayImageOptions.Builder()
												.showImageOnLoading(R.drawable.none)
												.cacheInMemory(true)
												.cacheOnDisk(true)
												.considerExifParams(true)
												.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
												.bitmapConfig(Bitmap.Config.RGB_565)
												.build();

		mTextViewTitle = (TextView) view.findViewById(R.id.text_view_title);
		mImageView = (ImageView) view.findViewById(R.id.image_view_photo);
//		mTextViewDescription = (TextView) view.findViewById(R.id.text_view_description);
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		
		Bundle args = getArguments();
		String title = args.getString("title");
		String image = args.getString("image");
//		String description = args.getString("description");
		
		mTextViewTitle.setText(title);
//		mTextViewDescription.setText(description);
		mImageLoader.displayImage(image, mImageView, mImageOptions);
	}

	@Override
	public FragmentId getFragmentId() {
		return FragmentId.MESSAGE_FROM_MEMBER;
	}

	@Override
	public String getTitle() {
		return getStr(R.string.label_smbc_nikkoushoken);
	}

	@Override
	public int getIcon() {
		return R.drawable.none;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// メニューなし
		menu.clear();
	}
}