package com.smbcnikkodisco.yell.android.fragment;

import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.smbcnikkodisco.yell.android.R;
import com.smbcnikkodisco.yell.android.common.ComHttp;
import com.smbcnikkodisco.yell.android.common.Const;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.smbcnikkodisco.yell.android.param.BaseParam;

/**
 * WebViewFragment
 * 
 * @author cayto inc.
 * 
 */
public final class WebViewFragment extends BaseFragment {

	private static final String _ACTION_BACK = "back";

	private String mTitle;
	private String mUrl;
	@SuppressWarnings("unused")
	private static final HashSet<String> _URL_SET;

	static {
		// 未定
		_URL_SET = new HashSet<String>();
	}

	// view
	private WebView mWebView;

	public WebViewFragment() {
		super();
		_TAG = TAG + " " + WebViewFragment.class.getSimpleName();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_web_view, container, false);
		mWebView = (WebView) view.findViewById(R.id.web_view);
		return view;
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);

		Bundle args = getArguments();
		mTitle = args.getString("title");
		mUrl = args.getString("link");

		// internet接続確認 ネットに接続されていない And ローカルアドレスでない
		if (!ComHttp.isConnected(getMainActivity().getApplicationContext())
		&&	!mUrl.startsWith(Const.LOCAL_URL_)) {
			mUrl = Const.LOCAL_URL_ERROR;
		}
		// タイトル更新
		active();
		// webview設定
		mWebView.resumeTimers();
//		mWebView.setWebViewClient(new CustomWebViewClient());
		mWebView.setWebViewClient(new WebViewClient());
//		mWebView.setWebChromeClient(new CustomWebChromeClient());
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setUseWideViewPort(true);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		// Application キャッシュ OFF
		mWebView.getSettings().setAppCacheEnabled(false);
		// HTML JS CSS キャッシュさせない
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		getMainActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (TextUtils.equals(Const._DEV, "production")) {
					// production
					mWebView.loadUrl(mUrl);
				} else if (TextUtils.equals(Const._DEV, "dev")) {
					// develop
					String param = "{\"show_status\":\"dev\"}";
					mWebView.postUrl(mUrl,param.getBytes());
				} else {
					// production
					mWebView.loadUrl(mUrl);
				}
			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mWebView != null) {
			mWebView.pauseTimers();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mWebView != null) {
			mWebView.resumeTimers();
		}
	}

	@Override
	public void onDestroy() {
		try {
			mWebView.stopLoading();
			getMainActivity().hideProgress();
			mWebView.setWebChromeClient(null);
			mWebView.setWebViewClient(null);
			unregisterForContextMenu(mWebView);
			ViewGroup parent = (ViewGroup) mWebView.getParent();
			if (parent != null) {
				parent.removeView(mWebView);
			}
			mWebView.destroy();
			mWebView = null;
		} catch (Exception e) {
		}
		super.onDestroy();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			getMainActivity().onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public FragmentId getFragmentId() {
		return FragmentId.WEB_VIEW;
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	@Override
	public int getIcon() {
		return R.drawable.none;
	}

	/*
	 * WebViewClient
	 */
	private final class CustomWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			getMainActivity().showProgress();
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView webView, String url) {
			// logger.debug("shouldOverrideUrlLoading " + url);
			if (url.startsWith(Const._URI_API.toString())) {
				// 新たにWebViewを開かない
				return false;
			}
			if (url.startsWith("file:")) {
				// file系アクセスは新たにWebViewを開かない
				return false;
			}
			Uri uri = Uri.parse(url);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// プログレス非表示
			getMainActivity().hideProgress();
			mWebView.requestFocus(View.FOCUS_DOWN);
		}
	}

	private final class CustomWebChromeClient extends WebChromeClient {
		public void onProgressChanged(WebView view, int progress) {
			if (progress > 85) {
				// progressを85%で閉じる
				if (getMainActivity().isProgress()) {
					getMainActivity().hideProgress();
				}
			}
		}
	}
}