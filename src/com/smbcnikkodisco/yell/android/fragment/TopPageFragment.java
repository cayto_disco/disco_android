/**
 * 
 */
package com.smbcnikkodisco.yell.android.fragment;

import static com.smbcnikkodisco.yell.android.common.Const.API_URL_POST_COUNT;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_PARAM;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_URL;
import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import com.smbcnikkodisco.yell.android.common.ComHttp;
import com.smbcnikkodisco.yell.android.common.Const;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.smbcnikkodisco.yell.android.common.HttpJsonLoader;
import com.smbcnikkodisco.yell.android.dao.CompanyMessageLogDao;
import com.smbcnikkodisco.yell.android.dao.MemberMessageLogDao;
//import com.smbcnikkodisco.yell.android.entity.CompanyMessageLog;
//import com.smbcnikkodisco.yell.android.param.ParamCompanyMessageList;
//import com.smbcnikkodisco.yell.android.param.ParamCompanyMessageList.Res.ResContent;
import com.smbcnikkodisco.yell.android.param.ParamTopNotification;

import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
//import android.text.TextUtils;
//import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smbcnikkodisco.yell.android.R;

/**
 * TopPageFragment
 * 
 * @author cayto inc.
 */
public final class TopPageFragment extends BaseFragment implements OnClickListener {

	private static HashMap<Integer,Pair<String, String>> webViewMap = new HashMap<>();
	private static HashMap<String, String> webViewUrlMap = new HashMap<>();
	// Company Badge Frame
	private FrameLayout mBadgeCompanyFrameLayout;
	private TextView mBadgeNumberCompanyTextView;
	
	// Member Badge Frame
	private FrameLayout mBadgeMemberFrameLayout;
	private TextView mBadgeNumberMemberTextView;

	static {
		webViewMap = new HashMap<>();
		//-- 金融企業・証券業界の基礎知識 --
		webViewMap.put(R.id.image_button_knowlage,new Pair<>("金融業界・証券業界の基礎知識", "finance"));
		//-- 就活応援コンテンツ --
		webViewMap.put(R.id.image_button_support,new Pair<>("キャリア応援コンテンツ", "career"));
		//-- アプリアンケート --
		webViewMap.put(R.id.image_button_questionnaire,new Pair<>("アプリアンケート", "question"));
		//-- ホームページ
		webViewMap.put(R.id.button_new_graduate,new Pair<>("ホームページ", "homepage"));
		//-- マイページ --
		webViewMap.put(R.id.button_mypage,new Pair<>("マイページ", "mypage"));
		//-- 日経就職ナビ --
		webViewMap.put(R.id.button_nikkei_navi,new Pair<>("日経就職ナビ", "https://sjob.nikkei.co.jp/2016/top/"));
	}
	
	public static TopPageFragment newInstance() {
		return new TopPageFragment();
	}
	
	public TopPageFragment() {
		super();
		_TAG = TAG + " " + TopPageFragment.class.getSimpleName();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_top_page, container, false);
		//-- 企業からのメッセージ --
		((ImageButton) view.findViewById(R.id.image_button_message_from_company)).setOnClickListener(this);
		
		//-- 内定者メッセージ --
		((ImageButton) view.findViewById(R.id.image_button_message_from_member)).setOnClickListener(this);
		
		//-- 金融企業・証券業界の基礎知識 --
		((ImageButton) view.findViewById(R.id.image_button_knowlage)).setOnClickListener(this);
		
		//-- 就活応援コンテンツ --公開予定
		((ImageButton) view.findViewById(R.id.image_button_support)).setOnClickListener(this);

		//-- アプリアンケート --
		((ImageButton) view.findViewById(R.id.image_button_questionnaire)).setOnClickListener(this);
		
		//セミナーの日程情報と予約
		((LinearLayout) view.findViewById(R.id.layout_seminar)).setOnClickListener(this);
		
		//-- ホームページ --
		((Button) view.findViewById(R.id.button_new_graduate)).setOnClickListener(this);
		
		//-- マイページ --公開予定
		((Button) view.findViewById(R.id.button_mypage)).setOnClickListener(this);
		
		//-- 日経就職ナビ --
		((Button) view.findViewById(R.id.button_nikkei_navi)).setOnClickListener(this);
		
		// Company badge Frame
		mBadgeCompanyFrameLayout = (FrameLayout)view.findViewById(R.id.badge_company_frameLayout); 
		// Company badge Number
		mBadgeNumberCompanyTextView = (TextView) view.findViewById(R.id.badge_number_company_textView);
		
		// Member badge Frame
		mBadgeMemberFrameLayout = (FrameLayout)view.findViewById(R.id.badge_member_frameLayout); 
		// Member badge Number
		mBadgeNumberMemberTextView = (TextView) view.findViewById(R.id.badge_number_member_textView);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		// internet接続確認
		if (!ComHttp.isConnected(getMainActivity().getApplicationContext())){
			Log.d("disco","Not Access Internet!!");
			addErrorFragment();
		} else {
			setBadgeNotification();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// メニューなし
		menu.clear();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}

	@Override
	public void onClick(View v) {
		Bundle args = new Bundle();
		Pair<String, String> pair = null;
		String link = "";
		switch (v.getId()) {
			// 企業からのメッセージ
			case R.id.image_button_message_from_company:
				// internet接続確認
				if (!ComHttp.isConnected(getMainActivity().getApplicationContext())) {
					addErrorFragment();
					break;
				}
				addFragment(FragmentId.MESSAGE_LIST_FROM_COMPANY);
				break;
			// 内定者からのメッセージ
			case R.id.image_button_message_from_member:
				// internet接続確認
				if (!ComHttp.isConnected(getMainActivity().getApplicationContext())) {
					addErrorFragment();
					break;
				}
				addFragment(FragmentId.MESSAGE_LIST_FROM_MEMBER);
				break;
			// セミナー情報
			case R.id.layout_seminar:
				// internet接続確認
				if (!ComHttp.isConnected(getMainActivity().getApplicationContext())) {
					addErrorFragment();
					break;
				}
				addFragment(FragmentId.SEMINAR_LIST);
				break;
				
			//	日経就職ナビ
			case R.id.button_nikkei_navi:
				args.putString("title", "日経就職ナビ");
				args.putString("link", "https://sjob.nikkei.co.jp/2016/top/");
				showConfirmOpenBrowserDialog(this, args);
				break;
			// アプリアンケート // マイページ
			case R.id.image_button_questionnaire:case R.id.button_mypage:
				pair = webViewMap.get(v.getId());
				link = webViewUrlMap.get(pair.second);
				args.putString("title", pair.first);
				args.putString("link", link);
				showConfirmOpenBrowserDialog(this, args);
				break;
			default:
				pair = webViewMap.get(v.getId());
				link = webViewUrlMap.get(pair.second);
				args.putString("title", pair.first);
				args.putString("link", link);
				addFragment(FragmentId.WEB_VIEW, args);
				break;
		}

//		if (fragmentId != null) {
//			addFragment(fragmentId, args);
//		}
	}

	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
		// アニメーションなし
		return AnimationUtils.loadAnimation(getMainActivity(), R.anim.fragment_slide_non);
	}

	@Override
	public void active() {
		super.active();
		setBadgeNotification();
	}
	
	@Override
	public FragmentId getFragmentId() {
		return FragmentId.TOP_PAGE;
	}

	@Override
	public String getTitle() {
		return getStr(R.string.label_smbc_nikkoushoken);
	}

	@Override
	public int getIcon() {
		return R.drawable.none;
	}
	
	private void addErrorFragment(){
		Bundle args = new Bundle();
		args.putString("title", "");
		args.putString("link", Const.LOCAL_URL_ERROR);
		addFragment(FragmentId.WEB_VIEW, args);
	}
	
	
	/**
	 * batch処理を追加
	 */
	private void setBadgeNotification() {
		
		// API通信用Parameter生成
		final ParamTopNotification prm = new ParamTopNotification();
		
		// Json型に置き換えてStringに格納 : parameter圧縮型
		String req = prm.req.toJson();
		
		final ArrayList<String> companyNotificationIds = new ArrayList<String>();
		final ArrayList<String> memberNotificationIds = new ArrayList<String>();
		
		// APIに投げるための Bundleを生成
		Bundle args = new Bundle();
		
		// API:ITEMS0010  URL:/items/category
		args.putString(HTTP_KEY_URL, API_URL_POST_COUNT);	// URL
		args.putString(HTTP_KEY_PARAM, req);					// Request Parameter

		/** 非同期通信ひな形 */
		getMainActivity().getSupportLoaderManager().restartLoader(API_URL_POST_COUNT.hashCode(), args, new LoaderCallbacks<JSONObject>() {
			
			/** 通信開始 */
			@Override
			public Loader<JSONObject> onCreateLoader(int id, Bundle args) {
				return new HttpJsonLoader(getMainActivity().getApplicationContext(), args);	// Jsonで非同期通信
			}

			/**　通信レスポンス */
			@Override
			public void onLoadFinished(Loader<JSONObject> loader, JSONObject json) {
				// json型から配列へ自動格納
				prm.res = prm.res.toEntity(json);

				// Company Notice 処理
				String[] companyPostIds = prm.res.postId.companyPostId.split("\\,");
				for (String str : companyPostIds ) {
					companyNotificationIds.add(str);
				}
				int companyLogSize =  new CompanyMessageLogDao(getActivity()).findSizeByMessageIds(companyNotificationIds);
				int companyPostSize = prm.res.postCount.companyPostCount;
				
				if (companyPostSize > companyLogSize) {
					mBadgeCompanyFrameLayout.setVisibility(View.VISIBLE);
					String badgeCompanyNumber = (companyPostSize-companyLogSize) > 9 ? "9+" : String.valueOf(companyPostSize-companyLogSize);
					mBadgeNumberCompanyTextView.setText(badgeCompanyNumber);
				} else {
					mBadgeCompanyFrameLayout.setVisibility(View.GONE);
				}
				
				// Member Notice 処理
				String[] employeePostIds = prm.res.postId.employeePostId.split("\\,");
				for (String str : employeePostIds ) {
					memberNotificationIds.add(str);
				}
				int memberLogSize =  new MemberMessageLogDao(getActivity()).findSizeByMessageIds(memberNotificationIds);
				int memberPostSize = prm.res.postCount.employeePostCount;
				
				if (memberPostSize > memberLogSize) {
					mBadgeMemberFrameLayout.setVisibility(View.VISIBLE);
					String badgeMemberNumber = (memberPostSize-memberLogSize) > 9 ? "9+" : String.valueOf(memberPostSize-memberLogSize);
					mBadgeNumberMemberTextView.setText(badgeMemberNumber);
				} else {
					mBadgeMemberFrameLayout.setVisibility(View.GONE);
				}
				
				// Url セットする				
				webViewUrlMap.put("finance", prm.res.contentsData.finance);
				webViewUrlMap.put("career", prm.res.contentsData.career);
				webViewUrlMap.put("question", prm.res.contentsData.question);
				webViewUrlMap.put("mypage", prm.res.contentsData.mypage);
				webViewUrlMap.put("homepage", prm.res.contentsData.homepage);
			}

			/** 通信失敗　キャンセル */
			@Override
			public void onLoaderReset(Loader<JSONObject> arg0) {}
		});
	}
}