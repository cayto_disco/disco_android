package com.smbcnikkodisco.yell.android.fragment;

import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import com.smbcnikkodisco.yell.android.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.smbcnikkodisco.yell.android.common.Const;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.smbcnikkodisco.yell.android.content.NutraBaseImageDecoder;
import com.smbcnikkodisco.yell.android.dao.MemberMessageLogDao;
import com.smbcnikkodisco.yell.android.entity.MemberMessageLog;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class ImagePagerFragment extends BaseFragment {

	public static final int INDEX = 2;

	private ViewPager mViewPager;

	private ImageLoader mImageLoader;
	private DisplayImageOptions mImageOptions;
	
	public ImagePagerFragment(){
		super();
		_TAG = TAG + ImagePagerFragment.class.getSimpleName();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mImageLoader = ImageLoader.getInstance();
		mImageOptions = new DisplayImageOptions.Builder()
												.showImageOnLoading(R.drawable.none)
												.cacheInMemory(true)
												.cacheOnDisk(true)
												.considerExifParams(true)
												.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
												.bitmapConfig(Bitmap.Config.RGB_565)
												.build();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_image_pager, container, false);
		mViewPager = (ViewPager) view.findViewById(R.id.pager);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		Bundle args = getArguments();
		int defaultPosition = args.getInt("position");
		final ArrayList<String> imageUrls = args.getStringArrayList("images");
		final ArrayList<String> itemIds = args.getStringArrayList("itemIds");
		final ArrayList<String> itemTitles = args.getStringArrayList("titles");
		final ArrayList<String> itemMessages = args.getStringArrayList("messages");
		mViewPager.setAdapter(new ImageAdapter(imageUrls,itemTitles,itemMessages));
		mViewPager.setCurrentItem(getArguments().getInt(Const.Extra.IMAGE_POSITION, defaultPosition));
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {	
			@Override
			public void onPageSelected(int position) {
				MemberMessageLog memberMessageLog = new MemberMessageLog();
				memberMessageLog.message_id = itemIds.get(mViewPager.getCurrentItem());
				// 既読ログを保存
				new MemberMessageLogDao(getMainActivity().getApplicationContext()).create(memberMessageLog);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}

			@Override
			public void onPageScrollStateChanged(int arg0) {}
		});
		
	}

	private class ImageAdapter extends PagerAdapter {

		private LayoutInflater inflater;
		private ArrayList<String> imgaeUrls = new ArrayList<String>();
		private ArrayList<String> itemTitles = new ArrayList<String>();
		private ArrayList<String> itemMessages = new ArrayList<String>();

		ImageAdapter(ArrayList<String> imgaeUrls, ArrayList<String> itemTitles, ArrayList<String> itemMessages) {
			inflater = LayoutInflater.from(getActivity());
			this.imgaeUrls = imgaeUrls;
			this.itemTitles = itemTitles;
			this.itemMessages = itemMessages;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return imgaeUrls.size();
		}
		
		public String getItemUrls(int position){
			return imgaeUrls.get(position);
		}

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
			// タイトル
			final TextView titleTextView = (TextView) imageLayout.findViewById(R.id.text_view_title);
			titleTextView.setText(itemTitles.get(position));
			// メッセージ
			final TextView messageTextView = (TextView) imageLayout.findViewById(R.id.text_view_description);
			messageTextView.setText(itemMessages.get(position));
			
			assert imageLayout != null;
			final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
			final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
			
			mImageLoader.displayImage(getItemUrls(position), imageView, mImageOptions, new SimpleImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

					spinner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}
				
			});

			view.addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}

	@Override
	public FragmentId getFragmentId() {
		return FragmentId.IMAGE_PAGER;
	}

	@Override
	public String getTitle() {
		return "";
	}

	@Override
	public int getIcon() {
		return R.drawable.none;
	}
}