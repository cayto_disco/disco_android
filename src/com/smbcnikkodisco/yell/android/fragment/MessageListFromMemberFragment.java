package com.smbcnikkodisco.yell.android.fragment;

import static com.smbcnikkodisco.yell.android.common.Const.API_URL_MEMBER_MESSAGE_LIST;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_PARAM;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_URL;
import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.util.ArrayList;

import com.smbcnikkodisco.yell.android.R;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.smbcnikkodisco.yell.android.common.HttpJsonLoader;
import com.smbcnikkodisco.yell.android.dao.MemberMessageLogDao;
import com.smbcnikkodisco.yell.android.entity.MemberMessageLog;
import com.smbcnikkodisco.yell.android.param.ParamSeniorMessageList;
import com.smbcnikkodisco.yell.android.param.ParamSeniorMessageList.Res.ResContent;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.json.JSONObject;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.WrapperListAdapter;

/**
 * MessageListFromMemberFragment
 * 
 * @author cayto inc.
 * 
 */
public final class MessageListFromMemberFragment extends BaseFragment {

	// view
	private ListView mListViewMassages;
	private int mNextCurrent;
	private int mMaxPage;
	private FrameLayout mItemListLoadFooter;
	private boolean mLoadingFlg = true;	// 読み込み中フラグ
	private ArrayList<String> mImages;	// メッセージのメイン画像
	private ArrayList<String> mItemIds;	// メッセージのIds
	private ArrayList<String> mItemTitles;	// メッセージのタイトルs
	private ArrayList<String> mItemMessages;	// メッセージのタイトルs
	
	public MessageListFromMemberFragment() {
		super();
		mImages = new ArrayList<String>();
		mItemIds= new ArrayList<String>();
		mItemTitles= new ArrayList<String>();
		mItemMessages= new ArrayList<String>();
		mNextCurrent = 1;
		mMaxPage = 1;
		_TAG = TAG + MessageListFromMemberFragment.class.getSimpleName();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_message_list_from_member, container, false);
		mListViewMassages = (ListView) view.findViewById(R.id.list_view_messages);
		// ロード用フッター
		mItemListLoadFooter = (FrameLayout) inflater.inflate(R.layout.item_list_load_footer, null);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		AsyncTask<String, Void, Object> task = new AsyncTask<String, Void, Object>() {
			@Override
			protected Object doInBackground(String... res) {
				try {
					initView();
				} catch (Exception e) {
					return e;
				}
				return null;
			}

			@Override
			protected void onPostExecute(Object result) {
				if (result instanceof Exception) {}
			}
		};

		task.execute();
		
//		GLSurfaceView mGLSurfaceView = new GLSurfaceView(getActivity());
//		mGLSurfaceView.queueEvent(new Runnable() {
//			public void run() {
//				initView();
//			}
//		});
	}

	@Override
	public void reload() {
		super.reload();
		mNextCurrent = 1;
		initView();
	}
	
	@Override
	public void active() {
		super.active();
		initView();
	}

	@Override
	public FragmentId getFragmentId() {
		return FragmentId.MESSAGE_LIST_FROM_MEMBER;
	}

	@Override
	public String getTitle() {
		return getStr(R.string.label_member_message);
	}

	@Override
	public int getIcon() {
		return R.drawable.none;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// メニューなし
		menu.clear();
	}

	private void initView() {
		mListViewMassages.addFooterView(mItemListLoadFooter);
		// Adapter生成
		MessageListAdapter adapter = new MessageListAdapter(getMainActivity().getApplicationContext(), R.layout.item_list_message_from_member, new ArrayList<ResContent>());
		// Listにセット
		((ListView) mListViewMassages).setAdapter(adapter);
		// クリック時の処理
		mListViewMassages.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (view.equals(mItemListLoadFooter)) {
					return;
				}
				
				Log.d("disco","position:"+String.valueOf(position));
				
				ResContent content = (ResContent) parent.getItemAtPosition(position);
				
				MemberMessageLog memberMessageLog = new MemberMessageLog();
				memberMessageLog.message_id = content.id;

				// 既読ログを保存
				new MemberMessageLogDao(getMainActivity().getApplicationContext()).create(memberMessageLog);
				
				Bundle args = new Bundle();
				args.putInt("position", position);
				args.putStringArrayList("titles", mItemTitles);
				args.putStringArrayList("itemIds", mItemIds);
				args.putStringArrayList("images", mImages);
				args.putStringArrayList("messages", mItemMessages);
				addFragment(FragmentId.IMAGE_PAGER, args);
			}
		});
		// ページングロード
		mListViewMassages.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView absListView, int i) {
			}
			// スクロール時
			@Override
			public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// 全アイテム数が表示されてた数と等しい&読み込み中ではない＆最大ページ未満なら次ページ取得
				if (totalItemCount == firstVisibleItem + visibleItemCount && !mLoadingFlg && mNextCurrent <= mMaxPage) {
					mLoadingFlg = true;
					mListViewMassages.addFooterView(mItemListLoadFooter);
					findMessages();	// 検索
				}
			}
		});
		
		mNextCurrent = 1;
		mMaxPage = 1;
		findMessages();
	}
	
	private void findMessages() {
		
		// API通信用Parameter生成
		final ParamSeniorMessageList prm = new ParamSeniorMessageList();
		
		// parameter詳細
		prm.req.current = getNextPageValueOfString();	// 現在のpage
		
		// Json型に置き換えてStringに格納 : parameter圧縮型
		String req = prm.req.toJson();
		
		// APIに投げるための Bundleを生成
		Bundle args = new Bundle();
		
		// API:ITEMS0010  URL:/items/category
		args.putString(HTTP_KEY_URL, API_URL_MEMBER_MESSAGE_LIST);	// URL
		args.putString(HTTP_KEY_PARAM, req);					// Request Parameter
		
		/** 非同期通信ひな形 */
		getMainActivity().getSupportLoaderManager().restartLoader(API_URL_MEMBER_MESSAGE_LIST.hashCode(), args, new LoaderCallbacks<JSONObject>() {
			
			/** 通信開始 */
			@Override
			public Loader<JSONObject> onCreateLoader(int id, Bundle args) {
				return new HttpJsonLoader(getMainActivity().getApplicationContext(), args);	// Jsonで非同期通信
			}

			/**　通信レスポンス */
			@Override
			public void onLoadFinished(Loader<JSONObject> loader, JSONObject json) {
				// フッター削除
				mListViewMassages.removeFooterView(mItemListLoadFooter);
				if (!checkParam(json)) {
					if(!mLoadingFlg && mNextCurrent <= mMaxPage){
						findMessages();
					}
					return;
				}
				// json型から配列へ自動格納
				prm.res = prm.res.toEntity(json);

				MessageListAdapter listViewMassageList = (MessageListAdapter)((WrapperListAdapter)mListViewMassages.getAdapter()).getWrappedAdapter();
				
				// 現在ページ設定
				setCurrentPage(prm.res.current);
				// Max Page 設定
				setMaxPage(prm.res.total_page);
				
				// 画像配列の初期化
				for(ResContent content : prm.res.contents) {
					listViewMassageList.add(content);
					mImages.add(content.image);
					mItemIds.add(content.id);
					mItemTitles.add(content.title);
					mItemMessages.add(content.description);
				}
				// 更新をListViewへ通知
				listViewMassageList.notifyDataSetChanged();
				// 読み込み中OFF
				mLoadingFlg = false;
			}

			@Override
			public void onLoaderReset(Loader<JSONObject> arg0) {
				mLoadingFlg = false;
				// フッター削除
				mListViewMassages.removeFooterView(mItemListLoadFooter);
			}
		});
	}
	
	private final static class MessageListAdapter extends ArrayAdapter<ResContent>{
		
		private ArrayList<ResContent> items;
		private int resourceId;
		private LayoutInflater inflater;
		private ImageLoader imageLoader;
		private DisplayImageOptions imageOptions;
		private Context context;
		
		public MessageListAdapter(Context context, int resourceId, ArrayList<ResContent> items) {
			super(context, resourceId, items);
			this.context = context;
			this.resourceId = resourceId;
			this.items = items;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = ImageLoader.getInstance();
			imageOptions = new DisplayImageOptions.Builder()
													.showImageOnLoading(R.drawable.none)
													.cacheInMemory(true)
													.cacheOnDisk(true)
													.considerExifParams(true)
													.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
													.bitmapConfig(Bitmap.Config.RGB_565)
													.build();
		}
		
		@Override
		public int getCount() {
			return items.size();
		}
		
		@Override
		public ResContent getItem(int position) {
			return items.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		 public void unregisterDataSetObserver(DataSetObserver observer) {
			if(observer != null){
				super.unregisterDataSetObserver(observer);
			}
		 }

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;
			ViewHolder holder;
			if (view == null) {
				view = inflater.inflate(resourceId, parent, false);
				holder = new ViewHolder();
				holder.linearLayout = (LinearLayout) view.findViewById(R.id.linear_layout_li);
				holder.textViewTitle = (TextView) view.findViewById(R.id.text_view_name);
				holder.imageViewPhoto = (ImageView) view.findViewById(R.id.image_view_photo);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			
			ResContent recordItem = getItem(position);
			if (recordItem != null) {
				
				// 既読処理
				int size =  new MemberMessageLogDao(context).findSizeByMessageId(recordItem.id);
				
				if (!TextUtils.equals(String.valueOf(size), "0")) {				
					holder.linearLayout.setBackgroundResource(R.color.gray_two_light);
				} else {
					holder.linearLayout.setBackgroundResource(R.color.white);
				}
				
				holder.textViewTitle.setText(recordItem.title);
				// アイコンセット
				imageLoader.displayImage(recordItem.thumbnail, holder.imageViewPhoto, imageOptions);
			}
			
			return view;
		}
		
	}
	
	private final static class ViewHolder {
		LinearLayout linearLayout;
		ImageView imageViewPhoto;
		TextView textViewTitle;
	}
	
	private String getNextPageValueOfString(){
		return String.valueOf(mNextCurrent);
	}
	
	private void setCurrentPage(String current){
		mNextCurrent++;
	};
	
	private void setMaxPage(int page){
		mMaxPage  = page;
	};
}