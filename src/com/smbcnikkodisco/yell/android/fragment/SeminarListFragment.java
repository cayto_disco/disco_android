package com.smbcnikkodisco.yell.android.fragment;

import static com.smbcnikkodisco.yell.android.common.Const.API_URL_SEMINAR_LIST;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_PARAM;
import static com.smbcnikkodisco.yell.android.common.Const.HTTP_KEY_URL;
import static com.smbcnikkodisco.yell.android.common.Const.TAG;

import java.util.ArrayList;

import com.smbcnikkodisco.yell.android.R;
import com.smbcnikkodisco.yell.android.common.FragmentId;
import com.smbcnikkodisco.yell.android.common.HttpJsonLoader;
import com.smbcnikkodisco.yell.android.param.ParamSeminarList;
import com.smbcnikkodisco.yell.android.param.ParamSeminarList.Res.ResContent;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.WrapperListAdapter;

/**
 * SeminarListFragment
 * 
 * @author cayto inc.
 * 
 */
public final class SeminarListFragment extends BaseFragment {

	// view
	private ListView mListViewMassages;
	
	// 現在のページ
	private int mNextCurrent;
	private int mMaxPage;
	private FrameLayout mItemListLoadFooter;
	private boolean mLoadingFlg = true;	// 読み込み中フラグ
	
	public SeminarListFragment() {
		super();
		mNextCurrent = 1;
		mMaxPage = 1;
		_TAG = TAG + MessageListFromCompanyFragment.class.getSimpleName();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_seminar_list, container, false);
		mListViewMassages = (ListView) view.findViewById(R.id.list_view_messages);
		// ロード用フッター
		mItemListLoadFooter = (FrameLayout) inflater.inflate(R.layout.item_list_load_footer, null);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		getMainActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				initView();
			}
		});
	}

	@Override
	public void reload() {
		super.reload();
		mNextCurrent = 1;
		initView();
	}

	@Override
	public FragmentId getFragmentId() {
		return FragmentId.SEMINAR_LIST;
	}

	@Override
	public String getTitle() {
		return getStr(R.string.label_smbc_nikkoushoken);
	}

	@Override
	public int getIcon() {
		return R.drawable.none;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// メニューなし
		menu.clear();
	}

	private void initView() {
		mListViewMassages.addFooterView(mItemListLoadFooter);
		// Adapter生成
		MessageListAdapter adapter = new MessageListAdapter(this, R.layout.item_list_seminar, new ArrayList<ResContent>());
		// Listにセット
		((ListView) mListViewMassages).setAdapter(adapter);
		// ページングロード
		mListViewMassages.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView absListView, int i) {
			}
			// スクロール時
			@Override
			public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				// 全アイテム数が表示されてた数と等しい&読み込み中ではない＆最大ページ未満なら次ページ取得
				if (totalItemCount == firstVisibleItem + visibleItemCount && !mLoadingFlg && mNextCurrent <= mMaxPage) {
					mLoadingFlg = true;
					mListViewMassages.addFooterView(mItemListLoadFooter);
					findMessages();	// 検索
				}
			}
		});
		
		mNextCurrent = 1;
		mMaxPage = 1;
		
		findMessages();
	}
	
	private void findMessages() {
		
		// API通信用Parameter生成
		final ParamSeminarList prm = new ParamSeminarList();
		
		// parameter詳細
		prm.req.current = getNextPageValueOfString();	// 現在のpage
		
		// Json型に置き換えてStringに格納 : parameter圧縮型
		String req = prm.req.toJson();
//		Log.d(TAG, "gson " + req);	// Log
		
		// APIに投げるための Bundleを生成
		Bundle args = new Bundle();
		
		// API:ITEMS0010  URL:/items/category
		args.putString(HTTP_KEY_URL, API_URL_SEMINAR_LIST);	// URL
		args.putString(HTTP_KEY_PARAM, req);					// Request Parameter

		/** 非同期通信ひな形 */
		getMainActivity().getSupportLoaderManager().restartLoader(API_URL_SEMINAR_LIST.hashCode(), args, new LoaderCallbacks<JSONObject>() {
			
			/** 通信開始 */
			@Override
			public Loader<JSONObject> onCreateLoader(int id, Bundle args) {
				return new HttpJsonLoader(getMainActivity().getApplicationContext(), args);	// Jsonで非同期通信
			}

			/**　通信レスポンス */
			@Override
			public void onLoadFinished(Loader<JSONObject> loader, JSONObject json) {
				// フッター削除
				mListViewMassages.removeFooterView(mItemListLoadFooter);
				
				if (!checkParam(json)) {
					if (!mLoadingFlg && mNextCurrent <= mMaxPage) {
						findMessages();
					}
					return;
				}
//				Log.d(TAG, json.toString());	// Log
				// json型から配列へ自動格納
				prm.res = prm.res.toEntity(json);

				// 現在ページ設定
				setCurrentPage(prm.res.current);
				// Max Page 設定
				setMaxPage(prm.res.total_page);
				
				MessageListAdapter listViewMassageList = (MessageListAdapter)((WrapperListAdapter)mListViewMassages.getAdapter()).getWrappedAdapter();
				
				for(ResContent content : prm.res.contents) {
					listViewMassageList.add(content);
				}
				// 更新をListViewへ通知
				listViewMassageList.notifyDataSetChanged();
				// 読み込み中OFF
				mLoadingFlg = false;
			}

			/** 通信失敗　キャンセル */
			@Override
			public void onLoaderReset(Loader<JSONObject> arg0) {
				// 読み込み中OFF
				mLoadingFlg = false;
				// フッター削除
				mListViewMassages.removeFooterView(mItemListLoadFooter);
			}
		});
	}
	
	private final static class MessageListAdapter extends ArrayAdapter<ResContent>{
		
		private ArrayList<ResContent> items;
		private int resourceId;
		private SeminarListFragment fragment;
		private LayoutInflater inflater;
		private Context context;
		
		public MessageListAdapter(SeminarListFragment fragment, int resourceId, ArrayList<ResContent> items) {
			super(fragment.getMainActivity().getApplicationContext(), resourceId, items);
			this.resourceId = resourceId;
			this.fragment = fragment;
			this.context = fragment.getMainActivity().getApplicationContext();
			this.items = items;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		public int getCount() {
			return items.size();
		}
		
		@Override
		public ResContent getItem(int position) {
			return items.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;
			ViewHolder holder;
			if (view == null) {
				view = inflater.inflate(resourceId, parent, false);
				holder = new ViewHolder();
				holder.textViewTitle = (TextView) view.findViewById(R.id.text_view_title);
				holder.buttonReserve = (Button) view.findViewById(R.id.button_reserve);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			final ResContent recordItem = getItem(position);
			
			if (TextUtils.equals(recordItem.link, "") || recordItem.link == null ) {
				holder.buttonReserve.setVisibility(View.GONE);
			}
			
			if (recordItem != null) {
				holder.textViewTitle.setText(recordItem.title);
				holder.buttonReserve.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle args = new Bundle();
						args.putString("title",recordItem.title);
						args.putString("link", recordItem.link);
						showConfirmOpenBrowserDialog(fragment, args);
					}
				});
			}
			
			return view;
		}
		
	}
	
	private final static class ViewHolder {
		TextView textViewTitle;
		Button buttonReserve;
	}

	private String getNextPageValueOfString(){
		return String.valueOf(mNextCurrent);
	}
	
	private void setCurrentPage(String current){
		mNextCurrent++;
	};
	
	private void setMaxPage(int page){
		mMaxPage  = page;
	};
}